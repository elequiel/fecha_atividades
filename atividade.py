import selenium
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


gg = webdriver.Chrome(".\\chromedriver.exe")
gg.get('http://intranet.sesisenaisp.org.br/Paginas/home.aspx')
try:
    element = WebDriverWait(gg, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="Tabela_01"]/tbody/tr/td[2]/a/img')))
    print('ok')
    element.click()
finally:
    print('acabou parte 1')


try:
    #painel = gg.find_element_by_xpath('/html/body/div[5]')
    print(str(gg.window_handles)) #Mostra as abas que possuem na janela
    gg.switch_to.window(gg.window_handles[-1])
    print(str(gg.current_window_handle)) #Mostra a janela atual
    element1 = WebDriverWait(gg, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[5]/div[3]')))
    element1.click()
finally:
    gg.refresh()
